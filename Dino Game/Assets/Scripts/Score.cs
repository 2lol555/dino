﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	int score;
	Text myText;
	void Start(){
		myText = GetComponent<Text>();
	}
	void Update () {
	}
	public void scorePoints(int scored){
		score += scored;
		myText.text = score.ToString();
	}
	public int returnScore(){
		return score;
	}
}
