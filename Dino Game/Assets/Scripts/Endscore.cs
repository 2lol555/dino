﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Endscore : MonoBehaviour {
	Text myText;
	int tempscore;

	void OnEnable(){
		tempscore = PlayerPrefs.GetInt("scoreNow");
	}

	void Start () {
		myText = GetComponent<Text>();
		myText.text = "Score: " + tempscore;
	}

	public int ReturnScore(){
		return tempscore;
	}
}
