﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {
	static float floorSpeed;
	public bool canSpawnObstacle;

	ObstacleSpawner spawner;
	public GameObject[] Obstacles;
	Score scoreLabel;

	void Start(){
		spawner = GameObject.FindObjectOfType<ObstacleSpawner>();
		scoreLabel = GameObject.FindObjectOfType<Score>();
		float currentScore = (float)scoreLabel.returnScore();
		floorSpeed = 15f + (0.7f * currentScore);
		if((int)Random.Range(0, 10) > 3){
		SpawnJumpObstacle(Random.Range(-3f, 3f));
		}
	}

	void FixedUpdate () {
		transform.position += (Vector3.left * (floorSpeed * 0.01f));
	}

	void OnTriggerExit2D(Collider2D collider){
		if(collider.tag == "Spawner")
		spawner.SpawnRoad();
	}
	void SpawnJumpObstacle(float x){
		if(canSpawnObstacle){
			GameObject currentspawn = Obstacles[Random.Range(0, Obstacles.Length)];
			var newObstacle = Instantiate(currentspawn, new Vector3(transform.position.x + x , transform.position.y, 0), Quaternion.identity);
			newObstacle.transform.parent = gameObject.transform;
		}
	}

}