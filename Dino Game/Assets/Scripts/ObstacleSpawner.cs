﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {
	public float roadspawnrate;
	public GameObject road;
	// Use this for initialization
	void Start () {
			
	}
	//void Update () {
	//}
	public void SpawnRoad(){
		var newRoad = Instantiate(road, new Vector3(transform.position.x , transform.position.y, 0), Quaternion.identity);
		newRoad.transform.parent = gameObject.transform;
	}
}
