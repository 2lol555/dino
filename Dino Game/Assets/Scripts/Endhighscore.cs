﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Endhighscore : MonoBehaviour {
	Endscore endscore;
	Text myText;
	int thisscore;
	int highscore;
	// Use this for initialization
	void Start () {
		myText = GetComponent<Text>();
		if(!PlayerPrefs.HasKey("Highscore")){
			PlayerPrefs.SetInt("Highscore", 0);
		}
		endscore = GameObject.FindObjectOfType<Endscore>();
		thisscore = endscore.ReturnScore();
		highscore = PlayerPrefs.GetInt("Highscore");
		if(highscore < thisscore){
			highscore = thisscore;
			PlayerPrefs.SetInt("Highscore", thisscore);
		}
		myText.text = "Highscore: " + highscore;
	}
}
