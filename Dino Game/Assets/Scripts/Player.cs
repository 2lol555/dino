﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public float jumpPower;
	public float boostPower;
	private bool canHighJump;
	private bool isCrouched;
	private bool HighJumpLimiter;
	Animator myAnimator;
	Rigidbody2D myRigidbody;
	Score ScoreLabel;

	void Start () {
		myAnimator = gameObject.GetComponent<Animator>();
		myRigidbody = transform.GetComponent<Rigidbody2D>();
		ScoreLabel = GameObject.FindObjectOfType<Score>();
		canHighJump = true;
	}
	//Inputs managed 
	void FixedUpdate () {
		//if(Input.GetKey(KeyCode.Space)){
		//	Up();
		//}
		//if(!Input.GetKey(KeyCode.Space)){
		//	Down();
		//}
		if(Input.GetMouseButton(0)){
				Up();
			}
		if(!Input.GetMouseButton(0)){
				Down();
			}
		}
	void Up(){
		if(canHighJump){
			if(!HighJumpLimiter){
				Debug.Log("Applying up force");
				HighJumpLimiter = true;
				Invoke("resetLimiter", 0.1f);
				myRigidbody.AddForce(Vector3.up * (jumpPower * myRigidbody.mass * myRigidbody.gravityScale * 20f));
				canHighJump = false;
				myAnimator.SetBool("IsJumping", true);
			}
		}
		myRigidbody.AddForce(Vector3.up * (boostPower * myRigidbody.mass * myRigidbody.gravityScale));
	}
	void Down(){
		if(!canHighJump){
			myRigidbody.AddForce(Vector3.down * (boostPower * myRigidbody.mass * myRigidbody.gravityScale));
		}
		//else{
		//	CrouchToggle();
		//}
	}
	void CrouchToggle(){
		if(!isCrouched){
			myAnimator.SetBool("IsCrouching", true);
		}else{
			myAnimator.SetBool("IsCrouching", false);
		}
		isCrouched = !isCrouched;
	}

	//Collision handling
	void OnCollisionEnter2D(){
		canHighJump = true;
		myAnimator.SetBool("IsJumping", false);
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Cleared"){
			ScoreLabel.scorePoints(1);
		}
	}

	void resetLimiter(){
		canHighJump = false;
		HighJumpLimiter = false;
	}

}
