﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Barrier : MonoBehaviour {
	LevelManager levelmanager;
	Score score;
	void Start(){
		levelmanager = GameObject.FindObjectOfType<LevelManager>();
		score = GameObject.FindObjectOfType<Score>();
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Player"){
			int scoreValue = score.returnScore();
			PlayerPrefs.SetInt("scoreNow", scoreValue);
			levelmanager.loadNextLevel();
		}
	}
}
