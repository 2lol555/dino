﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class LevelManager : MonoBehaviour {
	public float loadIn;
	public bool loadAds;
	public bool IsDevBuild;
	private string gameID =  "1724330";
	void Start(){
		if(loadAds){
			Advertisement.Initialize(gameID, true);
		}
		if(SceneManager.GetActiveScene().buildIndex == 0){
			if(loadIn != 0f){
				Invoke("loadNextLevel", loadIn);
			}
		}
	}

	public void loadNextLevel(){
		int currentLevelId = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(currentLevelId + 1);
	}
	public void loadLevel(int id){
		SceneManager.LoadScene(id);
	}
	public void QuitGame(){
		Application.Quit();
	}
}
